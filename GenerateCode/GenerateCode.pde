ArrayList<String> lines;

float Z_UP = 10;
float Z_INK = -60;
float Z_STENCIL =-30;
float Z_JUMP = 5;

// position of the first stencil
float X_STENCIL_1 = 90;
float Y_STENCIL_1 = -68;
// position of the second stencil
float X_STENCIL_2 = X_STENCIL_1 + 110;
float Y_STENCIL_2 = Y_STENCIL_1;
// Radius of the circle
float R_STENCIL = 10;

// Radius of the ink
float R_INK = 5;

// Number of repetitions
int SECTIONS_STENCIL = 9;
int SECTIONS_INK = 7;

// Position of the ink
float X_INK = 200;
float Y_INK = 115;



void setup(){
	lines = new ArrayList<String>();

	// Set Feed Rate
	setFeedRate(10000);

	/*
	// Set travel Z
	travelZ();

	// Move to ink
	lineXY(X_INK, Y_INK);
	*/

	// Repeat cycle
	for(int i=0; i<1; i++){

	// Get ink
	sectionedCircle(X_INK,Y_INK, R_INK, SECTIONS_INK,Z_INK);

	// Return to travel Z
	travelZ();


	// Create circle 1
	sectionedCircleWithCircles(X_STENCIL_1, Y_STENCIL_1, R_STENCIL, SECTIONS_STENCIL);

	// Return to travel Z
	travelZ();

	// Create circle 2
	sectionedCircleWithCircles(X_STENCIL_2, Y_STENCIL_2, R_STENCIL, SECTIONS_STENCIL);

	// Set travel Z
	travelZ();
	// Move to ink
	lineXY(X_INK, Y_INK);

	}


	for(String l : lines){
		println(l);
	}

	String [] s = new String[lines.size()];
	for(int i=0; i<s.length; i++){
		s[i] = lines.get(i);
	}

	saveStrings("file.nc",s);
	saveStrings("../XYZSerial/file.nc",s);
	exit();

}



String g1xy(float x, float y){
	return String.format("G1 X%.4f Y%.4f",x,y);
}

String g1z(float z){
	return String.format("G1 Z%.4f",z);
}

String g3xyr(float x, float y, float r){
	return String.format("G3 X%.4f Y%.4f R%.4f",x,y,r);

}

String feed(float f){
	return String.format("F%.4f",f);
}

void setFeedRate(float f){
	lines.add(feed(f));
}

void jumpZ(){
	stencilZ();
	stencilJumpZ();
}

void travelZ(){
	lines.add(g1z(Z_UP));
}

// Set Z to stencil height
void stencilZ(){
	lines.add(g1z(Z_STENCIL));
}

// Set Z to "jump" position
void stencilJumpZ(){
	lines.add(g1z(Z_STENCIL+Z_JUMP));
}

void lineXY(float x, float y){
	lines.add(g1xy(x,y));
}

void lineZ(float z){
	lines.add(g1z(z));
}

// XY coordinates of next point, radius of the circle
void circleXYR_CCW(float x, float y, float r){
	lines.add(g3xyr(x,y,r));
}

// Left x, left y, radius, sections
void sectionedCircle(float cx, float cy, float r, int n, float z){
	float angleIncrement = 2*PI/n;
	float angle;
	float x,y;

	x = cx+r;
	y = cy;
	angle = 0;

	// Set Z to travel height
	travelZ();
	// Move to first point
	lineXY(x,y);
	// Z Down and Up
	lineZ(z);
	lineZ(z+Z_JUMP);

	for(angle=angleIncrement; angle<=2*PI; angle+=angleIncrement){
		x = cx + r*cos(angle);
		y = cy + r*sin(angle);

		// Circular interpolation to new point
		circleXYR_CCW(x,y,r);
		
		// Z Down and Up
		lineZ(z);
		lineZ(z+Z_JUMP);
	}

}


// 
// Draws a small circle for each section of the big circle
void sectionedCircleWithCircles(float cx, float cy, float r, int n){
	float angleIncrement = 2*PI/n;
	float angle, angleBefore;
	float x,y;

	float x2,y2;

	float cxs, cys; // Center small circle

	float xd1,yd1,xd2,yd2;
	// Radius to the target of the small circles
	// If 0, target is the center
	float r2 = 0.0*r;

	// Radius of the small circle
	float rs = 0.5*(r-r2);

	x = cx+r;
	y = cy;
	angle = 0;

	// Set Z to travel height
	travelZ();
	// Move to first point
	lineXY(x,y);
	
	// Z Down
	stencilZ();


	for(angle=angleIncrement; angle<=TWO_PI; angle+=angleIncrement){
		angleBefore = angle - angleIncrement;

		// Calculate center point of small circle
		cxs = cx + rs*cos(angleBefore);
		cys = cy + rs*sin(angleBefore);

		// Calculate point in the quarter circle trajectory
		// towards the big circle center
		xd1 = cxs - rs*cos(HALF_PI-angleBefore);
		yd1 = cys + rs*sin(HALF_PI-angleBefore);

		// Calculate target point in the half circle trajectory
		// Towards the big circle center
		// (If r2=0, x2,y2 is the big circle center)
		x2 = cx + r2*cos(angleBefore);
		y2 = cy + r2*sin(angleBefore);

		// Calculate point in the quarter circle trajectory
		// towards the point in the big circle
		xd2 = cxs + rs*cos(HALF_PI-angleBefore);
		yd2 = cys - rs*sin(HALF_PI-angleBefore);



		// Z down
		stencilZ();

		// Small circle divided in four quarters
		circleXYR_CCW(xd1,yd1,r/2);
		circleXYR_CCW(x2,y2,r/2);
		circleXYR_CCW(xd2,yd2,r/2);
		circleXYR_CCW(x,y,r/2);


		// Calculate next point in big circle
		x = cx + r*cos(angle);
		y = cy + r*sin(angle);

		// Circular interpolation to new point
		circleXYR_CCW(x,y,r);

		// Z up
		stencilJumpZ();
	}


}


