float R_EXT = 250;
float R_INT = 145;
float R_CIR = R_EXT-R_INT;
int N_DIV = 5;

void setup(){
	size(500,500);

	noFill();

}

void draw(){
	R_CIR = R_EXT-R_INT;

	background(255);

	stroke(100);
	translate(width/2,height/2);
	ellipse(0,0,R_EXT*2,R_EXT*2);
	ellipse(0,0,R_INT*2,R_INT*2);

	stroke(0);
	for(int i=0; i<N_DIV; i++){
		pushMatrix();
		rotate(TWO_PI*i/N_DIV);
		translate(R_INT,0);
		ellipse(0,0,5,5);
		ellipse(0,0,R_CIR*2,R_CIR*2);
		popMatrix();
	}

	stroke(255,0,0);
	float x, y;
	float a = 0;
	x = R_INT*cos(a);
	y = R_INT*sin(a);
	arc(x,y,R_CIR*2,R_CIR*2,0,PI*0.9);


}

void keyPressed(){
	switch(key){
		case 'j':
			R_INT--;
			println(R_INT);
			break;
		case 'k':
			R_INT++;
			println(R_INT);
			break;

	}

}
